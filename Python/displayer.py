import networkx
from networkx.readwrite import json_graph
import matplotlib.pyplot as pyplot
import json
import sys

def get_coordinates(G, node):
	return G.node[node]['lat'], G.node[node]['lon']

scaling_factor = 500

def adjust_coords(lat, lon):
	return lon * scaling_factor, lat * scaling_factor

if __name__ == '__main__':
	agent_lat, agent_lon, goal_lat, goal_lon = (float(arg) for arg in sys.argv[1:])

	# read graph data from dump
	f = open("map_dump.json", "r")
	G = json_graph.node_link_graph(json.load(f))
	f.close()

	# display graph
	networkx.draw_networkx_edges(G, pos={node : adjust_coords(*get_coordinates(G, node)) for node in G.nodes()})
	pyplot.plot(agent_lon*scaling_factor, agent_lat*scaling_factor, 'ro', markersize=5)
	pyplot.plot(goal_lon*scaling_factor, goal_lat*scaling_factor, 'go', markersize=5)
	pyplot.show()