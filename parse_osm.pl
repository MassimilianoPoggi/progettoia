:- use_module(library(sgml)).
:- use_module(library(xpath)).

parse_file(DOM) :- 
 	open('C:/Users/Massimiliano/Desktop/Navigatore/map.osm', read, Stream), 
 	load_xml(Stream, DOM, []),
 	close(Stream).

parse_dom(X, T) :-
	parse_file(DOM), 
	xpath(DOM, //T, X).

 main(N, T) :-
 	parse_dom(N, T).