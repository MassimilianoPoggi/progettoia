\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Conoscenza Statica}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}La mappa}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Creazione della mappa}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Display della mappa}{4}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Il traffico}{4}{section.2.2}
\contentsline {chapter}{\numberline {3}Conoscenza dinamica}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Correlazione traffico}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Stima del traffico e delle percorrenze}{6}{section.3.2}
\contentsline {chapter}{\numberline {4}Ciclo decidi pianifica esegui}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Logica implementativa}{7}{section.4.1}
\contentsline {section}{\numberline {4.2}Decisioni implementate}{7}{section.4.2}
\contentsline {chapter}{\numberline {5}Esempio di esecuzione}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Software necessari per l'esecuzione}{9}{section.5.1}
\contentsline {section}{\numberline {5.2}Esecuzione}{9}{section.5.2}
